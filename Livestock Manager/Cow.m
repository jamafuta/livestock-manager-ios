#import "Cow.h"


@implementation Cow

@dynamic name;
@dynamic tag;
@dynamic breed;
@dynamic dateOfBirth;
@dynamic colour;
@dynamic dateOfSell;
@dynamic owner;
@dynamic gender;
@dynamic origin;
@dynamic comments;

@end
