#import <Foundation/Foundation.h>
#import "FXForms.h"
#import "MJMangedObjectModelForm.h"
#import "MJValidateForm.h"


@interface MJCowForm : NSObject <FXForm, MJMangedObjectModelForm, MJValidateForm>
@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSString *tag;
@property(nonatomic, retain) NSString *breed;
@property(nonatomic, retain) NSDate *dateOfBirth;
@property(nonatomic, retain) NSString *colour;
@property(nonatomic, retain) NSDate *dateOfSell;
@property(nonatomic, retain) NSString *owner;
@property(nonatomic, retain) NSString *gender;
@property(nonatomic, retain) NSString *origin;
@property(nonatomic, retain) NSString *comments;
@property(nonatomic, retain) NSManagedObjectID *id;
@end