#import "MJCowForm.h"
#import "Cow.h"


@implementation MJCowForm {

}
- (NSManagedObject *)objectFromFormWithContext:(NSManagedObjectContext *)context {
    Cow *cow;
    if (self.id != nil) {
        cow = (Cow *) [context objectWithID:self.id];
    } else {
        cow = [NSEntityDescription
                insertNewObjectForEntityForName:@"Cow"
                         inManagedObjectContext:context];
    }

    cow.tag = self.tag;
    cow.name = self.name;
    cow.breed = self.breed;
    cow.comments = self.comments;
    cow.origin = self.origin;
    cow.gender = self.gender;
    cow.owner = self.owner;
    cow.dateOfSell = self.dateOfSell;
    cow.colour = self.colour;
    cow.dateOfBirth = self.dateOfBirth;

    return cow;
}

+ (MJCowForm *)formFromManagedObject:(Cow *)object {
    MJCowForm *mjCowForm = [[MJCowForm alloc] init];
    mjCowForm.breed = object.breed;
    mjCowForm.comments = object.comments;
    mjCowForm.origin = object.origin;
    mjCowForm.gender = object.gender;
    mjCowForm.owner = object.owner;
    mjCowForm.dateOfSell = object.dateOfSell;
    mjCowForm.colour = object.colour;
    mjCowForm.dateOfBirth = object.dateOfBirth;
    mjCowForm.tag = object.tag;
    mjCowForm.name = object.name;
    return mjCowForm;
}

- (NSArray *)fields {
    return @[@"tag", @"name", @"breed", @"dateOfBirth", @"colour", @"dateOfSell", @"owner",
            @{FXFormFieldKey : @"gender", FXFormFieldOptions : @[@"Bull", @"Cow"]},
            @{FXFormFieldKey : @"origin", FXFormFieldType : FXFormFieldTypeLongText},
            @{FXFormFieldKey : @"comments", FXFormFieldType : FXFormFieldTypeLongText}];
}

- (NSArray *)formErrors {
    NSMutableArray *errors = [[NSMutableArray alloc] init];
    if (self.tag.length <= 0) {
        [errors addObject:@"Tag is required"];
    }

    if (self.name.length <= 0) {
        [errors addObject:@"Name is required"];
    }


    return errors;
}


@end