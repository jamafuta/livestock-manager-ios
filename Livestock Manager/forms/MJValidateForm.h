//
// Created by James Muranga on 5/20/14.
// Copyright (c) 2014 muranga. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MJValidateForm <NSObject>
-(NSArray *)formErrors;
@end