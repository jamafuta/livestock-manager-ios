#import <Foundation/Foundation.h>

@protocol FXForm;

@protocol MJMangedObjectModelForm <NSObject>
- (NSManagedObject *)objectFromFormWithContext:(NSManagedObjectContext *)context;

+ (id <FXForm> )formFromManagedObject:(NSManagedObject *)object;
@end