//
//  MJDetailViewController.h
//  Livestock Manager
//
//  Created by James Muranga on 5/11/14.
//  Copyright (c) 2014 muranga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MJDetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
