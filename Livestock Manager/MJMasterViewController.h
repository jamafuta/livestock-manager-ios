#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@class FXFormViewController;

@interface MJMasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property(strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property(strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property(nonatomic, strong) FXFormViewController *controller;
@end
