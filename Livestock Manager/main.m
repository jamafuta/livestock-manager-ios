//
//  main.m
//  Livestock Manager
//
//  Created by James Muranga on 5/11/14.
//  Copyright (c) 2014 muranga. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MJAppDelegate class]));
    }
}
